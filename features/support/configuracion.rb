class Config
  CANTIDAD_ASIENTOS = 10
  CANTIDAD_VENTANAS = 3
  CURSO_X = 'curso x'.freeze
  DIA_LUNES = 'lunes'.freeze
  TURNO_TARDE = 'tarde'.freeze # Esto sigue siendo vigente?
  NOMBRE_AULA = 'a1'.freeze
  NO_REQUIERE_MESA = false
  TIPO_BANCOS = 'pupitre_individual'.freeze
end
