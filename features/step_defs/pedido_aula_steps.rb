require_relative '../support/configuracion'

Dada('un aula {string} con capacidad {int}') do |nombre_aula, capacidad|
  cuerpo_mensaje = {
    'nombre': nombre_aula,
    'asientos': capacidad,
    'ventanas': Config.CANTIDAD_VENTANAS,
    'tipo-bancos': Config.TIPO_BANCOS
  }
  post '/aulas', cuerpo_mensaje.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('un curso {string} con {int} alumnos el dia {string}') do |curso, alumnos, dia|
  @curso = curso
  @cupo_alumnos = alumnos
  @dia = dia
end

Dada('un aula {string} con mesa de trabajo con capacidad {int}') do |nombre_aula, capacidad|
  cuerpo_mensaje = {
    'nombre': nombre_aula,
    'asientos': capacidad,
    'ventanas': Config.CANTIDAD_VENTANAS,
    'tipo-bancos': 'mesa_de_trabajo'
  }
  post '/aulas', cuerpo_mensaje.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('un curso {string} con {int} alumnos el dia {string} que requiere mesa de trabajo') do |curso, alumnos, dia|
  @curso = curso
  @cupo_alumnos = alumnos
  @dia = dia
  @requiere_mesa = true
end

Cuando('pido un aula') do
  cuerpo_mensaje = {
    'curso': @curso || Config.CURSO_X,
    'cupo': @cupo_alumnos,
    'requiere_mesa_de_trabajo': @requiere_mesa || Config.NO_REQUIERE_MESA,
    'dia': @dia || Config.DIA_LUNES
  }
  post '/cursos', cuerpo_mensaje.to_json, 'CONTENT_TYPE' => 'application/json'
end

Entonces('el curso es asignado al aula {string}') do |aula_esperada|
  expect(last_response.status).to eq 201
  @aula_asignada = JSON.parse(last_response.body)['aula']
  expect(@aula_asignada).to eq aula_esperada
end

Entonces('el curso {string} es asignado al aula {string}') do |curso, aula_asignada|
  get "/curso/#{curso}"
  expect(last_response.status).to eq 200
  aula = JSON.parse(last_response.body)['aula']
  expect(aula).to eq aula_asignada
end

Entonces('el curso queda sin aula') do
  expect(last_response.status).to eq 400
  explicacion = JSON.parse(last_response.body)['explicacion']
  expect(explicacion).to eq 'NO_HAY_AULA_ACORDE_DISPONIBLE'
end
