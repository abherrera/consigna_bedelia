require_relative '../support/configuracion'

Dada('un aula {string} con {int} asientos') do |nombre_aula, cantidad_asientos|
  @nombre_aula = nombre_aula
  @cantidad_asientos = cantidad_asientos
end

Dada('{int} ventanas') do |cantidad_ventanas|
  @cantidad_ventanas = cantidad_ventanas
end

Dada('bancos tipo {string}') do |tipo_bancos|
  @tipo_bancos = tipo_bancos
end

Cuando('calculo su capacidad') do
  cuerpo_mensaje = {
    'nombre': @nombre_aula || Config.NOMBRE_AULA,
    'asientos': @cantidad_asientos || Config.CANTIDAD_ASIENTOS,
    'ventanas': @cantidad_ventanas || Config.CANTIDAD_VENTANAS,
    'tipo-bancos': @tipo_bancos || Config.TIPO_BANCOS
  }
  post '/aulas', cuerpo_mensaje.to_json, 'CONTENT_TYPE' => 'application/json'
end

Entonces('obtengo {int}') do |capacidad_esperada|
  expect(last_response.status).to eq 201
  @capacidad_aula = JSON.parse(last_response.body)['capacidad']
  expect(@capacidad_aula).to eq capacidad_esperada
end

Entonces('obtengo un error') do
  expect(last_response.status).to be == 400
  error = JSON.parse(last_response.body)['error']
  expect(error).not_to be_nil
end
