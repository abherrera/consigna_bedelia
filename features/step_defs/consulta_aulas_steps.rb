Cuando('consulto las aulas') do
  get '/aulas'
end

Entonces('obtengo {int} aulas') do |_int|
  expect(last_response.status).to be == 200
  aulas = JSON.parse(last_response.body)
  expect(aulas.size).to eq 3
end
