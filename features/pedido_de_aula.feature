#language: es
Característica: Pedido de aula

  Antecedentes:
    Dado que existe un aula "a1" con capacidad 200
    Dado que existe un aula "a2" con capacidad 20
    Dado que existe un aula "a4" con mesa de trabajo con capacidad 100

  @wip
  Escenario: pa01 - Se asigna el primer aula acorde disponible 
    Dado un curso "algo1" con 10 alumnos el dia "lunes"
    Cuando pido un aula
    Entonces el curso es asignado al aula "a1"

  @wip
  Escenario: pa02 - No se asigna aula si no hay aula acorde
    Dado un curso "algo1" con 500 alumnos el dia "lunes"
    Cuando pido un aula
    Entonces el curso queda sin aula

  @wip
  Escenario: pa03 - Curso con mesa de trabajo requieren aula con mesa de trabajo
    Dado un curso "algo1" con 10 alumnos el dia "martes" que requiere mesa de trabajo
    Cuando pido un aula
    Entonces el curso es asignado al aula "a4"

  @wip
  Escenario: pa04 - Dos cursos en el mismo aula pero distinto dia
    Dado un curso "algo1" con 150 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "algo2" con 150 alumnos el dia "martes"
    Cuando pido un aula
    Entonces el curso "algo2" es asignado al aula "a1"
    Entonces el curso "algo1" es asignado al aula "a1"

  @wip
  Escenario: pa05 - Dos cursos en el mismo día pero distinta aula
    Dado un curso "algo1" con 150 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "algo2" con 10 alumnos el dia "lunes"
    Cuando pido un aula
    Entonces el curso "algo1" es asignado al aula "a1"
    Entonces el curso "algo2" es asignado al aula "a2"

  @wip
  Escenario: pa06 - No se asigna aula para día domingo
    Dado un curso "algo1" con 500 alumnos el dia "domingo"
    Cuando pido un aula
    Entonces el curso queda sin aula

  @wip
  Escenario: pa07 - No se asigna aula por no haber disponible en el mismo día
    Dado un curso "algo1" con 150 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "memo2" con 148 alumnos el dia "lunes"
    Cuando pido un aula
    Entonces el curso queda sin aula

  @wip
  Escenario: pa08 - No se asigna aula con mesa de trabajo por no haber disponible en el mismo dia
    Dado un curso "algo1" con 10 alumnos el dia "lunes" que requiere mesa de trabajo
    Y pido un aula
    Y un curso "algo2" con 10 alumnos el dia "lunes" que requiere mesa de trabajo
    Cuando pido un aula
    Entonces el curso queda sin aula
    
  @wip  
  Escenario: pa09 - No se asigna aula por asignacion lineal en el mismo día
    Dado un curso "algo1" con 10 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "memo2" con 25 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "algo2" con 30 alumnos el dia "lunes"
    Cuando pido un aula
    Entonces el curso queda sin aula

  @wip
  Escenario: pa10 - Se asigna aula con mesa de trabajo a dos cursos en distintos días
    Dado un curso "algo1" con 10 alumnos el dia "lunes" que requiere mesa de trabajo
    Y pido un aula
    Y un curso "algo2" con 10 alumnos el dia "martes" que requiere mesa de trabajo
    Cuando pido un aula
    Entonces el curso "algo1" es asignado al aula "a4"
    Entonces el curso "algo2" es asignado al aula "a4"

  @wip
  Escenario: pa11 - Se asigna aula con mesa de trabajo a curso que no la necesita
    Dado un curso "algo1" con 20 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "memo2" con 20 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "taller1" con 20 alumnos el dia "lunes"
    Cuando pido un aula
    Entonces el curso "taller1" es asignado al aula "a4"

  @wip
  Escenario: pa12 - No se asigna aula con mesa de trabajo despues de ser ocupada por curso sin necesidad de mesa de trabajo
    Dado un curso "algo1" con 100 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "algo2" con 100 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "memo1" con 20 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "memo2" con 25 alumnos el dia "lunes"
    Y pido un aula
    Y un curso "ayc1" con 40 alumnos el dia "lunes" que requiere mesa de trabajo
    Cuando pido un aula
    Entonces el curso queda sin aula
