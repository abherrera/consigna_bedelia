#language: es
Característica: Consulta de curso

  Antecedentes:
    Dado que existe un aula "a1" con capacidad 20

  Escenario: cc01 - Consulta de curso con aula asignada
    Dado un curso "algo1" con 10 alumnos el dia "lunes"
    Y pido un aula
    Cuando consulto el curso "algo1"
    Entonces el curso es asignado al aula "a1"

  Escenario: cc02 - No se asigna aula si no hay aula acorde
    Dado un curso "algo2" con 30 alumnos el dia "lunes"
    Y pido un aula
    Cuando consulto el curso "algo1"
    Entonces el curso no tiene aula
